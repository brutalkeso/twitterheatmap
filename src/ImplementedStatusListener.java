import twitter4j.*;


public class ImplementedStatusListener implements StatusListener {
	private final String[] parties = {"S", "M", "SD", "MP","C","V","FP","KD"};
	private String[][] keywords;
	double[][] locations= {{54.997552, 9.431415}, {69.127995, 24.900164}};
	
	public ImplementedStatusListener(String[][] keywords) {
		this.keywords=keywords;
	}
	
	public void onStatus(Status status) {
    	GeoLocation loc = status.getGeoLocation();
    	if(loc!=null){
    		double dloc[] = {loc.getLatitude(), loc.getLongitude()};
    		System.out.println("Found status");
    		System.out.println(status.getText());
    		System.out.println(loc.getLatitude() +" "+ loc.getLongitude());
    		
    		if(dloc[0]>locations[0][0] && dloc[1]>locations[0][1] && dloc[0]<locations[1][0] && dloc[1]<locations[1][1]){
    			System.out.println("in sweden!");
	    		//Checks which party is associated with the tweet
	    		
	    		for(int i=0;i<parties.length;i++){
	    			for(int j=0;j<keywords[i].length;j++){
	    				if(status.getText().toLowerCase().contains(keywords[i][j].toLowerCase())){
	    					Tweet curTweet = new Tweet(status.getText(),parties[i], dloc, status.getCreatedAt(), status.getId());
	    					System.out.println("User: " + status.getUser().getName());
	    					System.out.println("Party: " + parties[i]);
	    					System.out.println("Keyword: " + keywords[i][j]);
	    					System.out.println("Tweet: " + status.getText());
	    					System.out.println("Time: " + status.getCreatedAt());
	    					System.out.println("-------------------------------------------------");
	    					
	    					Database.add(curTweet);
	    				}
	    			}
	    		}
    		}
    	}
    }
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}
    public void onException(Exception ex) {
        ex.printStackTrace();
    }
	public void onScrubGeo(long userId, long upToStatusId) {
		// TODO Auto-generated method stub			
	}
	public void onStallWarning(StallWarning warning) {
		// TODO Auto-generated method stub			
	}

}
