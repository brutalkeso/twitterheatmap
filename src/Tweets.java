import java.util.ArrayList;

/**
 * Created by marc on 2014-12-06.
 */
public class Tweets {
    ArrayList<Tweet> tweets;

    public Tweets() {
        tweets = new ArrayList<Tweet>();
    }

    public void add(Tweet tweet) {
        tweets.add(tweet);
    }
}
