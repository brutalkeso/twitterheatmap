import java.io.*;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * Created by marc on 2014-12-06.
 */
public class Database {
    private volatile static ArrayList<Tweet> database;
    private volatile static ArrayList<Tweet> latestUpdate;
    private static final String filepath = "C:\\wamp\\www\\database\\database.txt";

    public Database() {

    }

    public static void createDatabase() {
        latestUpdate = new ArrayList<Tweet>();
        readDatabase();

        System.out.println("printing database");
        for(Tweet t : database) {
            System.out.println(t.getText());
        }
    }

    public synchronized static void add(Tweet tweet) {
        System.out.println("adding tweet to database");
        database.add(tweet);
        latestUpdate.add(tweet);
        saveDatabase();
    }

    public static ArrayList<ArrayList<Tweet>> splitAndGet() {
        ArrayList<ArrayList<Tweet>> returnList = new ArrayList<ArrayList<Tweet>>();
        ArrayList<Tweet> halfOfList = new ArrayList<Tweet>();
        ArrayList<Tweet> otherHalfOfList = new ArrayList<Tweet>();
        int i;
        for(i = 0; i < database.size() / 2; i++) {
            halfOfList.add(database.get(i));
        }
        for(int j = i; j < database.size(); j++) {
            otherHalfOfList.add(database.get(j));
        }
        returnList.add(halfOfList);
        returnList.add(otherHalfOfList);
        return returnList;
    }

    /**
     * Creates json string from full database
     * @return full database as JSON string
     */
    public synchronized static String fullList() {
        return getAsJSON(database);
    }

    public synchronized static String onlyUpdate() {

        if(!latestUpdate.isEmpty()) {
            return getAsJSON(latestUpdate);
        }
        return null;
    }

    public synchronized static String getAsJSON(ArrayList<Tweet> list) {
        JSONObject object = new JSONObject();
        ArrayList<JSONObject> listOfTweets = new ArrayList<JSONObject>();

        for(Tweet t : list) {
            listOfTweets.add(t.createJSON());
        }
        object.put("listOfTweets", listOfTweets);

        //reset latest update
        latestUpdate = new ArrayList<Tweet>();
        return object.toJSONString();
    }

    private static void saveDatabase() {

        try {
            JSONObject object = new JSONObject();
            object.put("numberOfTweets", database.size());

            for(int i = 0; i < database.size(); i++) {
                object.put(Integer.toString(i), database.get(i).createJSON());
            }
            File f = new File(filepath);
            if(!f.exists()) {
                f.createNewFile();
            }

            FileWriter output= new FileWriter(f);
            BufferedWriter writer=new BufferedWriter(output);
            writer.append(object.toJSONString());
            writer.close();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void readDatabase() {
        database = new ArrayList<Tweet>();
        File f = new File(filepath);
        try {
            FileReader reader = new FileReader(f);
            JSONObject o = (JSONObject) (new JSONParser()).parse(reader);
            Long nrOfTweets = (Long) o.get("numberOfTweets");
            System.out.println("nrOfTweets: " + nrOfTweets);

            for(int i = 0; i < nrOfTweets; i++) {
                Tweet t = new Tweet((JSONObject) o.get(Integer.toString(i)));
                database.add(t);
            }
        } catch (FileNotFoundException e) {
            System.out.println("found no file");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
