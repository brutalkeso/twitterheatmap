import twitter4j.TwitterException;

/**
 * Created by marc on 2014-12-06.
 */
public class UpdateThread{

    private volatile boolean alive;
    private Database database;

    private Connector connector;
    private final String[] parties = {"S", "M", "SD", "MP","C","V","FP","KD"};
    private String[][] keywords = {
            {"socialdemokrat", "ssu", "löfven", "sosse", "sossar", "rödgrön", "@socialdemokrat", "@StefanLfven", "@Regeringschefen"},
            {"Reinfeldt", "moderaterna", "muf", "borgare", "borgarna", "alliansen","@nya_moderaterna","@freinfeldt", "@alliansswe"},
            {"Åkesson", "sverigedemokrat", "SD","@jimmieakesson", "@sdriks", },
            {"Miljöparti", "Fridolin","Romson", "Grönungdom", "Grön ungdom", "rödgrön", "@miljopartiet", "@GustavFridolin", "@asaromson"},
            {"Centerparti", "Centern", "Lööf","CUF", "borgare", "borgarna", "alliansen", "@alliansswe", "@centerpartiet", "@cuf", "@annieloof"},
            {"Vänsterparti", "VPK", "Sjöstedt", "Ungvänster", "ung vänster", "rödgrön","@vansterpartiet", "@jssjostedt", "vänstern"},
            {"Folkparti", "Björklund", "FP", "borgare", "borgarna", "alliansen", "@alliansswe", "@folkpartiet", "@janbjorklund"},
            {"Kristdemokrat", "KDU", "Hägglund", "borgare", "borgarna", "alliansen", "@alliansswe", "@kdriks", "@goranhagglund"}
    };

    public UpdateThread(Object database) throws TwitterException {
    	connector = new Connector(keywords);
    	//connector.getOldTweets();
        alive = true;
        this.database = (Database) database;
    }

    private Tweet getInfoFromTwitter() {
    	
        //get info from twitter based on something
        return null;
    }
    public static void main(String[] args){
    	Database db= new Database();
    	try {
			UpdateThread test = new UpdateThread(db);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void kill() {
        alive = false;
    }


}


