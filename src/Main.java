import java.io.IOException;

/**
 * Created by marc on 2014-12-06.
 */
public class Main {

    public static void main(String args[]) {
        MainServer s = null;
        Database.createDatabase();
        try {
            s = new MainServer();
            s.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        s.killServer();
    }
}
