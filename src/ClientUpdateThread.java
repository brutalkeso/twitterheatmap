import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by marc on 2014-12-06.
 */
public class ClientUpdateThread extends Thread {
    private boolean alive;
    private Socket socket;
    private DataOutputStream outputStream;

    public ClientUpdateThread(Socket s) {
        alive = true;
        this.socket = s;

        try {
            outputStream = new DataOutputStream(s.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        String tweets;

        while(alive) {

            try {
                Thread.sleep(1000);
                if((tweets = Database.onlyUpdate()) != null) {
                    System.out.println("found, sending to client");
                    outputStream.writeUTF(tweets);
                    System.out.println("sent");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
                alive = false;
            } catch (IOException e) {
                e.printStackTrace();
                alive = false;
            }
        }
    }

    public void kill(){
        alive = false;

        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
