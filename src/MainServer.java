import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import twitter4j.TwitterException;

/**
 * Created by marc on 2014-12-06.
 */
public class MainServer extends Thread{

    private Database database;
    private UpdateThread updateThread;
    private ServerSocket serverSocket;
    private boolean alive;
    private ArrayList<ClientUpdateThread> clients;

    public MainServer() throws IOException {
        serverSocket = new ServerSocket(7777);

        alive = true;
        clients = new ArrayList<ClientUpdateThread>();
        database = new Database();
        try {
			updateThread = new UpdateThread(database);
		} catch (TwitterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    public void run() {

        while(alive) {

            try {
                System.out.println("Socket starting, waiting for connection");
                Socket s  = serverSocket.accept();

                //write full database to client
                System.out.println("Client connected");
                DataOutputStream s2 = new DataOutputStream(s.getOutputStream());
                ArrayList<ArrayList<Tweet>> a = Database.splitAndGet();
                s2.writeUTF(Database.getAsJSON(a.get(0)));
                s2.writeUTF(Database.getAsJSON(a.get(1)));
                System.out.println("successfull");

                //create and start thread updating client IRT
                ClientUpdateThread t = new ClientUpdateThread(s);
                System.out.println("starting update thread");
                clients.add(t);
                clients.get(clients.indexOf(t)).start();
                System.out.println("finished");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void killServer() {

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        alive = false;
        updateThread.kill();

        for(ClientUpdateThread t : clients) {
            t.kill();
        }
    }
}