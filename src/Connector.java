

import twitter4j.*;

import java.util.ArrayList;
import java.util.List;

public class Connector {
	private Twitter twitter;
	private TwitterStream stream;
	private StatusListener listener;
	private String party;
	private String[][] keywords;
	private User user;
	private final String[] parties = {"S", "M", "SD", "MP","C","V","FP","KD"};
	double[][] locations= {{54.997552, 9.431415}, {69.127995, 24.900164}};
	int tweetCount=0;
	
	
	public Connector(String[][] keywords) throws TwitterException{
		this.keywords=keywords;
		twitter = new TwitterFactory().getInstance();
		
		//Open stream and set listener
		stream=new TwitterStreamFactory().getInstance();
		
		//Listener properties
		listener = new ImplementedStatusListener(keywords);
	    stream.addListener(listener);
	    FilterQuery filter = new FilterQuery();
	    
	    
	    //Filters out tweets from sweden only;
	    int counter=0;
	    for(int i=0;i<keywords.length;i++){
	    	for(int j=0;j<keywords[i].length;j++){
	    		counter++;
	    	}
	    }
	    String[] keywords2=new String[counter];
	    counter=0;
	    for(int i=0;i<keywords.length;i++){
	    	for(int j=0;j<keywords[i].length;j++){
	    		keywords2[counter]=keywords[i][j];
	    		counter++;
	    	}
	    }
	    
	    filter.track(keywords2);
	    
	    //	    filter.locations(locations);
//	    54.997552, 9.431415;
//	    69.127995, 24.900164
	    
	    
	    stream.filter(filter);
		user = twitter.verifyCredentials();
	}
	
	public void getOldTweets() throws TwitterException{
		int counter = 0;
		int queries=0;
		for(int i=0;i<keywords.length;i++){
			for (int j=0;j<keywords[i].length;j++){
				Query query = new Query(keywords[i][j]);
				query.setCount(100);
				QueryResult result;
				for(int k=0;k<3;k++){
					if(query!=null) {
						result = twitter.search(query);
						queries++;
						System.out.println("Queries: " + queries);
						List<Status> resultStatus = result.getTweets();
						for (Status status : resultStatus) {
							Tweet current = tweetify(status);
							if (current != null) {
								counter++;
								System.out.println("Tweet no: " + counter);
								System.out.println("Tweet added");
								Database.add(current);
							}
						}
						query = result.nextQuery();
					}
				}
				
			}
		}
	}
	
	public Tweet tweetify(Status status){
		Tweet curTweet=null;
		GeoLocation loc = status.getGeoLocation();
    	if(loc!=null){
    		double dloc[] = {loc.getLatitude(), loc.getLongitude()};
    		System.out.println("Found status");
    		System.out.println(status.getText());
    		System.out.println(loc.getLatitude() +" "+ loc.getLongitude());
    		
    		if(dloc[0]>locations[0][0] && dloc[1]>locations[0][1] && dloc[0]<locations[1][0] && dloc[1]<locations[1][1]){
    			System.out.println("in sweden!");
	    		//Checks which party is associated with the tweet
	    		
	    		for(int i=0;i<parties.length;i++){
	    			for(int j=0;j<keywords[i].length;j++){
	    				if(status.getText().toLowerCase().contains(keywords[i][j].toLowerCase())){
	    					tweetCount++;
	    					curTweet= new Tweet(status.getText(),parties[i], dloc, status.getCreatedAt(), status.getId());
	    					System.out.println("User: " + status.getUser().getName());
	    					System.out.println("Party: " + parties[i]);
	    					System.out.println("Keyword: " + keywords[i][j]);
	    					System.out.println("Tweet: " + status.getText());
	    					System.out.println("Time: " + status.getCreatedAt());
	    					System.out.println("-------------------------------------------------");
	    				}
	    			}
	    		}
    		}
    	}
    	return curTweet;
		
	}
	public void kill(){
		stream.clearListeners();
		stream.cleanUp();
	}
	
//	private void getTweets(String user) throws TwitterException{
//		Query query = new Query("#svpol");
//		List<Status> timeline = twitter.getUserTimeline(user);
////		QueryResult result = twitter.search(query);
//		for(Status current: timeline){
//			if(current.getRetweetCount()>0){
//				ResponseList<Status> retweets = twitter.getRetweets(current.getId());
//				if(retweets!=null){
//					System.out.println("Retweets: " + retweets.size());
//					for(Status retweet: retweets){
//						User retweeter = retweet.getUser();
//						System.out.println("\tUser: " + retweeter.getName() + " Location: " + retweeter.getLocation());
//						
//					}
//				}
//			}
//		}
//		
//		
//		//Print tweets
////		for (Status status : result.getTweets()) {
////	        System.out.println("@" + status.getUser().getScreenName() + ":" + status.getRetweetCount());
////	    }
//	}
}
